# Atlassian Connect Workflow Post Function Example

This add-on demonstrates simple usage of a workflow post function for Atlassian Connect.

Get started:

1. Start up a local copy of JIRA following the [developing locally](https://developer.atlassian.com/static/connect/docs/developing/developing-locally.html) guide.
2. Ensure you have `node.js` installed, then run `npm install` to install dependencies.
3. Run `node app.js` to start and install the add-on
4. Add the workflow post function to the TEST project (you may have to create it).

There is some inline documentation about using workflow post functions which you can see when adding the post function to a project.

For more information, read the Atlassian Connect [documentation on workflow post functions](https://developer.atlassian.com/static/connect/docs/modules/jira/workflow-post-function.html).