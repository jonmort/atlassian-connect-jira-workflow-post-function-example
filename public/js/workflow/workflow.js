$(document).ready(function () {
    var codeDefaults = {
        mode: "javascript",
        theme: "neat",
        lineNumbers:true,
        gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
        indentWithTabs:false,
        electricChars:true,
        indentUnit:2,
        lineWrapping: true,
        readOnly:true
    };

    $('pre code').each(function(){
        codify(this)
    });

    function codify(block) {
        var text = $(block).text().trim(),
            newOpts = $.extend({}, codeDefaults);

        if (text.split("\n").length <= 1) {
            newOpts.lineNumbers = false;
            newOpts.gutters = [];
        }
        newOpts.value = text;

        var cm = CodeMirror(
            function(node){
                block.parentNode.replaceChild(node, block)
            }, newOpts
        );
    }
    AP.require(["jira"], function(jira) {
        jira.WorkflowConfiguration.onSave(function() {
            var config = {
                "colour": "blue",
                "amount": 4
            }
            return "helloworld";
        });


        jira.WorkflowConfiguration.onSaveValidation(function() {
            return true;
        });
    });

    $('#config').html(document.location.hash);
});
